import pandahouse

connection = {
    'host': 'HOST_NAME',
    'password': 'PASSWORD',
    'user': 'USER',
    'database': 'DATABASE'
}

q = 'SELECT * FROM {db}.feed_actions where toDate(time) = today() limit 10'

df = pandahouse.read_clickhouse(q, connection=connection)

print(df.head())
