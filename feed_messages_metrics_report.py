import pandas as pd
import numpy
import telegram
import io
import pandahouse as ph
from datetime import date, timedelta
import matplotlib.pyplot as plt
import seaborn as sns
import os

sns.set(rc={'figure.figsize': (16, 9)}, 
        style="whitegrid")



def feed_message_metrics_report(chat=None):
    # Вводим токен нашего бота
    bot = telegram.Bot(token=os.environ.get("REPORT_BOT_TOKEN"))

    # Сохраняю id чата
    chat_id = chat or -727662986

    # Подключаюсь к базе данных
    connection = {'host': 'http://clickhouse.beslan.pro:8080',
                          'database':'simulator_20220120',
                          'user':'student', 
                          'password':'dpo_python_2020'
                         }

    # Выгружаю данные
    connection = {'host': 'http://clickhouse.beslan.pro:8080',
                          'database':'simulator_20220120',
                          'user':'student', 
                          'password':'dpo_python_2020'
                         }

    query_messages = '''
    SELECT
        toDate(message_time) AS date,
        count(message_user_id) AS messages
    FROM
        (SELECT
          A.user_id AS message_user_id,
          A.time AS message_time
        FROM
          simulator_20220120.message_actions AS A
        LEFT SEMI JOIN 
          simulator_20220120.feed_actions AS B 
          ON A.user_id = B.user_id)
    WHERE
        toDate(message_time) <= yesterday()
        AND
        toDate(message_time) >= yesterday() - 7
    GROUP BY
        date
    '''

    query_feed_mess = '''
    SELECT
        toDate(feed_time) AS date,
        count(DISTINCT feed_user_id) AS dau,
        countIf(feed_user_id, action='view') AS views,
        countIf(feed_user_id, action='like') AS likes,
        countIf(feed_user_id, action='like') / countIf(feed_user_id, action='view') AS ctr
    FROM
        (SELECT
          user_id AS feed_user_id,
          action,
          time AS feed_time
        FROM
          simulator_20220120.feed_actions AS A
        LEFT SEMI JOIN 
          simulator_20220120.message_actions AS B 
          ON A.user_id = B.user_id)
    WHERE
        toDate(feed_time) <= yesterday()
        AND
        toDate(feed_time) >= yesterday() - 7
    GROUP BY
        date
    '''

    query_feed_mess_mau = '''
    SELECT
        count(DISTINCT user_id) AS users
    FROM
        (SELECT
          user_id,
          time
        FROM
          simulator_20220120.feed_actions AS A
        LEFT SEMI JOIN 
          simulator_20220120.message_actions AS B 
          ON A.user_id = B.user_id)
    WHERE
        toDate(time) <= yesterday()
        AND
        toDate(time) >= yesterday() - 30
    '''

    messages = ph.read_clickhouse(query_messages, connection=connection)
    metrics = ph.read_clickhouse(query_feed_mess, connection=connection)
    mau = ph.read_clickhouse(query_feed_mess_mau, connection=connection)

    # Форматирую данные в столбце CTR
    metrics['ctr'] = metrics.ctr.apply(lambda x: round((x * 100), 2))

    # Сохраняю ключевые метрики за вчерашний день в отдельные переменные
    today = date.today()
    yesterday = today - timedelta(days = 1)
    dau = metrics.dau.tolist()[-1]
    mau = mau.users.tolist()[0]
    stickiness = round((dau / mau * 100), 2)
    views = metrics.views.tolist()[-1]
    likes = metrics.likes.tolist()[-1]
    ctr = metrics.ctr.tolist()[-1]
    mes = messages.messages.tolist()[-1]
    
    # Формирую текстовое сообщение с метриками
    msg = ('Значения метрик по пользователям, использующих новостную ленту и сообщения' + '\n'
           'за ' + str(yesterday) + ':' + '\n' + '\n'
           'DAU: ' + str(dau) + '\n'
           'MAU: ' + str(mau) + '\n'
           'Stickiness: ' + str(stickiness) + '%' + '\n'
           'CTR: ' + str(ctr) + '%' + '\n'
           'Количество просмотров: ' + str(views) + '\n'
           'Количество лайков: ' + str(likes) + '\n'
           'Количество сообщений: ' + str(mes))

    # Отправляю сообщение с метриками
    bot.sendMessage(chat_id=chat_id, text=msg)


    # Создаю графики и отправляю от имени бота
    # DAU
    sns.lineplot(metrics.date, metrics.dau)
    plt.title('DAU пользователей, использующих новостную ленту и сообщения', fontsize=16)
    plt.ylabel('Количество пользователей', fontsize=12)
    plt.xlabel('')
    # Чтобы не сохранять файл в файловой системе, воспользуемся пакетом IO. 
    # Он позволяет использовать буфер обмена в качестве хранилища данных.
    plot_object_dau = io.BytesIO()
    plt.savefig(plot_object_dau)
    plot_object_dau.name = 'dau_plot.png'
    plot_object_dau.seek(0)
    plt.close()

    # CTR
    sns.lineplot(metrics.date, metrics.ctr)
    plt.title('CTR пользователей, использующих новостную ленту и сообщения, %', fontsize=16)
    plt.ylabel('')
    plt.xlabel('')
    plot_object_ctr = io.BytesIO()
    plt.savefig(plot_object_ctr)
    plot_object_ctr.name = 'ctr_plot.png'
    plot_object_ctr.seek(0)
    plt.close()

    # Просмотры
    sns.lineplot(metrics.date, metrics.views)
    plt.title('Количество просмотров от пользователей, использующих новостную ленту и сообщения', fontsize=16)
    plt.ylabel('')
    plt.xlabel('')
    plot_object_views = io.BytesIO()
    plt.savefig(plot_object_views)
    plot_object_views.name = 'views_plot.png'
    plot_object_views.seek(0)
    plt.close()

    # Лайки
    sns.lineplot(metrics.date, metrics.likes)
    plt.title('Количество лайков от пользователей, использующих новостную ленту и сообщения', fontsize=16)
    plt.ylabel('')
    plt.xlabel('')
    plot_object_likes = io.BytesIO()
    plt.savefig(plot_object_likes)
    plot_object_likes.name = 'likes_plot.png'
    plot_object_likes.seek(0)
    plt.close()
    
    # Сообщения
    sns.lineplot(messages.date, messages.messages)
    plt.title('Количество сообщений от пользователей, использующих новостную ленту и сообщения', fontsize=16)
    plt.ylabel('')
    plt.xlabel('')
    plot_object_mess = io.BytesIO()
    plt.savefig(plot_object_mess)
    plot_object_mess.name = 'messages_plot.png'
    plot_object_mess.seek(0)
    plt.close()
    
    
    # Отправляем графики
    bot.sendPhoto(chat_id=chat_id, photo=plot_object_dau)
    bot.sendPhoto(chat_id=chat_id, photo=plot_object_ctr)
    bot.sendPhoto(chat_id=chat_id, photo=plot_object_views)
    bot.sendPhoto(chat_id=chat_id, photo=plot_object_likes)
    bot.sendPhoto(chat_id=chat_id, photo=plot_object_mess)

    
try:
    feed_message_metrics_report()
except Exception as e:
    print(e)
